package fi.jelinden.androiddevelopment2022

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fi.jelinden.androiddevelopment2022.adapter.CoffeeListAdapter
import fi.jelinden.androiddevelopment2022.data.CoffeeList


class MainActivity : AppCompatActivity() {
    private val coffeeList = CoffeeList
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var coffeesView: RecyclerView = findViewById(R.id.listCoffeesView)
        var columnCount = 1
        coffeesView.layoutManager = GridLayoutManager(this, columnCount)

        val data = coffeeList.getCoffees()

        coffeesView.adapter = CoffeeListAdapter(data)

        var addCoffeeBtn:Button = findViewById(R.id.addCoffeeBtn)
        addCoffeeBtn.setOnClickListener() {
            val intentToStart = Intent(this, AddCoffeeActivity::class.java)
            startActivity(intentToStart)
        }
    }
}