package fi.jelinden.androiddevelopment2022.adapter

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fi.jelinden.androiddevelopment2022.R
import fi.jelinden.androiddevelopment2022.data.Coffee

class CoffeeListAdapter(private val coffees: List<Coffee>): RecyclerView.Adapter<CoffeeListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        val weightTextView: TextView = itemView.findViewById(R.id.weightTextView)
        val priceTextView: TextView = itemView.findViewById(R.id.priceTextView)
        val roastlevelTextView: TextView = itemView.findViewById(R.id.roastlevelTextView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.coffees_listview_detail, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val coffee = coffees[position]

        viewHolder.nameTextView.text = coffee.name
        viewHolder.priceTextView.text = "Price: ".plus(coffee.price.toString()).plus("€")
        viewHolder.roastlevelTextView.text = "Roast level: ".plus(coffee.roastlevel.toString())
        viewHolder.weightTextView.text = "Weight: ".plus(coffee.weigth).plus("g")
    }

    override fun getItemCount() = coffees.size

}

