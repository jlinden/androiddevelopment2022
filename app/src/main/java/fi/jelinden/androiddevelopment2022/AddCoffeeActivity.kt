package fi.jelinden.androiddevelopment2022;

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import fi.jelinden.androiddevelopment2022.data.Coffee
import fi.jelinden.androiddevelopment2022.data.CoffeeList


class AddCoffeeActivity : AppCompatActivity() {
    private val coffeeList = CoffeeList
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.coffee_add_detail)

        var cancelBtn: Button = findViewById(R.id.cancelBtn)
        cancelBtn.setOnClickListener {
            val intentToStart = Intent(this, MainActivity::class.java)
            startActivity(intentToStart)
        }

        var spinner: Spinner = findViewById(R.id.roastLevelNumberField)
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item, arrayOf("1", "2", "3", "4", "5")
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.onItemSelectedListener

        var saveButton: Button = findViewById(R.id.saveCoffeeBtn)
        saveButton.setOnClickListener {

            val coffeeNameText: EditText = findViewById(R.id.coffeeNameTextField)
            val coffeeName: String = coffeeNameText.text.toString().trim()

            val coffeePriceText: EditText = findViewById(R.id.priceNumberField)
            val coffeePrice: Float = coffeePriceText.text.toString().trim().toFloat()

            val coffeeWeightText: EditText = findViewById(R.id.weightNumberField)
            val coffeeWeight: String = coffeeWeightText.text.toString().trim()

            val coffeeRoastLvlText: Spinner = findViewById(R.id.roastLevelNumberField)
            val coffeeRoastLvl: Int = coffeeRoastLvlText.selectedItem.toString().toInt()

            val newCoffee = Coffee(coffeeName, coffeePrice, coffeeWeight, coffeeRoastLvl)
            coffeeList.addCoffee(newCoffee)

        }
    }
}
