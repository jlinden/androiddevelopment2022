package fi.jelinden.androiddevelopment2022.data

data class Coffee(
    val name: String,
    val price: Float,
    val weigth: String,
    val roastlevel: Int
)