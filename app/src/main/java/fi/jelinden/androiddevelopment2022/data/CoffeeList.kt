package fi.jelinden.androiddevelopment2022.data

object CoffeeList {
    private var coffeeData: ArrayList<Coffee> = ArrayList()

    init {
    }

    fun getCoffees(): ArrayList<Coffee> {
        return coffeeData
    }

    fun addCoffee(coffee:Coffee) {
        this.coffeeData.add(coffee)
    }
}