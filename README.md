# Android Development 2022 / Jouni Linden

## Running

The easiest way is to run the project in Android studio emulator.
Import the project, add a new virtual or physical device and run.

You can also run `./gradlew test` to run tests.
There aren't any tests added though.

## Video link

* https://youtu.be/D4qUFpQXY_Q

## Learning diary

* You should be able to open it with this link:

[SD_learning_diary.pdf](https://bitbucket.org/jlinden/androiddevelopment2022/raw/4e7ccd166d5bea774be04f67d9726b41d2719837/SDS_learning_diary.pdf)


